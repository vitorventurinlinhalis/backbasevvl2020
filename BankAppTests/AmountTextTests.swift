//
//  AmountTextTests.swift
//  BankAppTests
//
//  Created by Vitor Venturin Linhalis on 15/08/2020.
//  Copyright © 2020 Vitor Venturin Linhalis. All rights reserved.
//

import XCTest
@testable import BankApp
 
class AmountTextTests: XCTestCase { // -- No views needed
    var amountTextVMmock_BRL: AmountTextViewModel!
    var amountTextVMmock_GBP: AmountTextViewModel!
    var amountTextVMmock_EUR: AmountTextViewModel!
    
    var localeMock_France = Locale(identifier: "fr_FR")
    var localeMock_Netherlands = Locale(identifier: "nl_NL")
    var localeMock_London = Locale(identifier: "en_GB")
    var localeMock_Brazil = Locale(identifier: "pt_BR")
    var localeMock_USA = Locale(identifier: "en_US")
    var localeMock_Europe = Locale(identifier: "en_EU")
    
    var amountOfMoneyMock_BRL = AmountText(value: 0.00, currencyCode: "BRL", currencySymbol: "R$")
    var amountOfMoneyMock_EUR = AmountText(value: 0.00, currencyCode: "EUR", currencySymbol: "€")
    var amountOfMoneyMock_USA = AmountText(value: 0.00, currencyCode: "DOL", currencySymbol: "$")
    var amountOfMoneyMock_GBP = AmountText(value: 0.00, currencyCode: "GBP", currencySymbol: "£")
    
    override func setUp() {
        amountTextVMmock_GBP = AmountTextViewModel(amount: amountOfMoneyMock_GBP)
        amountTextVMmock_BRL = AmountTextViewModel(amount: amountOfMoneyMock_BRL)
        amountTextVMmock_EUR = AmountTextViewModel(amount: amountOfMoneyMock_EUR)
    }

    // SOLVED: issue with space character -> https://developer.apple.com/forums/thread/5868
    func test_500EurosNegative() {
        amountTextVMmock_EUR.setAmount(to: -500.00)
        let sanitizedAmount = amountTextVMmock_EUR.sanitizedCurrencyString()
        XCTAssertEqual(sanitizedAmount, "−500,00€")
    }

    func test_1000EurosPositive() {
        amountTextVMmock_EUR.setAmount(to: 1000.00)
        let sanitizedAmount = amountTextVMmock_EUR.sanitizedCurrencyString()
        XCTAssertEqual(sanitizedAmount, "1.000,00€")
    }
    
    // Formatter
    func testFormatter_EuropeLocale_changeCurrencyCode() {
        Formatter.numberFormatter.locale = localeMock_Europe
        Formatter.numberFormatter.numberStyle = .currencyISOCode //change it to "EUR" instead of symbol
        amountTextVMmock_EUR.setAmount(to: 1000.00)
        let sanitizedAmount = amountTextVMmock_EUR.sanitizedCurrencyString()
        XCTAssertEqual(sanitizedAmount, "EUR1,000.00")
    }
    
    func testFormatter_LocaleIdentifierIsValid() {
        let fmt = Formatter.numberFormatter
        fmt.locale = localeMock_London
        XCTAssertEqual(fmt.locale.identifier, "en_GB")
    }
    
    func testFormatter_Pounds_LocaleIsValid() {
        XCTAssertEqual(localeMock_London.currencyCode, "GBP")
    }
    
    func testFormatter_EuropeLocale_changeCurrencyToPounds() {
        let fmt = Formatter.numberFormatter
        fmt.locale = localeMock_London
        fmt.numberStyle = .currency
        fmt.currencySymbol = amountTextVMmock_GBP.amount.currencySymbol
        fmt.currencyCode = amountTextVMmock_GBP.amount.currencyCode
        
        amountTextVMmock_GBP.setAmount(to: 1000.00)
        let sanitizedAmount = amountTextVMmock_GBP.sanitizedCurrencyString()
        XCTAssertEqual(sanitizedAmount, "£1,000.00")
    }
    
    func testFormatter_AmericaLocale_changeCurrencyTo10000Reais() {
        let fmt = Formatter.numberFormatter
        fmt.locale = localeMock_Brazil
        fmt.numberStyle = .currency
        fmt.currencySymbol = amountTextVMmock_BRL.amount.currencySymbol
        fmt.currencyCode = amountTextVMmock_BRL.amount.currencyCode
        
        amountTextVMmock_BRL.setAmount(to: 1000.00)
        let sanitizedAmount = amountTextVMmock_BRL.sanitizedCurrencyString()
        XCTAssertEqual(sanitizedAmount, "R$1.000,00")
    }
    
    // UI behavior
    func testUI_whenIsNegativeBalance_ComponentColorState() {
        amountTextVMmock_EUR.setAmount(to: -500.00)
        XCTAssertEqual(amountTextVMmock_EUR.textColor, UIColor.red)
    }

    func testUI_whenIsPositiveBalance_ComponentColorState() {
        amountTextVMmock_EUR.setAmount(to: 1000.00)
        XCTAssertEqual(amountTextVMmock_EUR.textColor, UIColor.black)
    }

    override func tearDown() {
        amountTextVMmock_GBP = nil
        amountTextVMmock_EUR = nil
        amountTextVMmock_BRL = nil
    }
    
}
