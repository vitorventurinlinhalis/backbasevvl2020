//
//  AccountTitleTests.swift
//  BankAppTests
//
//  Created by Vitor Venturin Linhalis on 15/08/2020.
//  Copyright © 2020 Vitor Venturin Linhalis. All rights reserved.
//

import XCTest
@testable import BankApp

class AccountTitleTests: XCTestCase {
    var accountTitleViewModelMock: AccountTitleViewModel!
    var accountTitleMock: AccountTitle!
    
    override func setUp() {
        accountTitleMock = AccountTitle(name: "Vitor Venturin", number: "NL81 ABNA 0212 4710 66")
        accountTitleViewModelMock = AccountTitleViewModel(accountTitle: accountTitleMock)
    }

    func testAccountTitle_isNameNotEmpty() {
        XCTAssertFalse(accountTitleMock.name.isEmpty)
    }
    
    override func tearDown() {
        accountTitleViewModelMock = nil
    }
}
