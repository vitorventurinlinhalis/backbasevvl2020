# Backbase test assignment #

## UI Engineer Assignment ##
Your task is to build a native application from scratch, using Xcode.
The application consists of 1 screen. A screenshot of the expected result is attached below.

![BankApp](https://bitbucket.org/vitorventurinlinhalis/backbasevvl2020/downloads/image1.jpg)

You do NOT have to include images or icons.
Colors and sizing should be generally respected but do NOT have to match perfectly.
However, the layout must be the same and the alignment between elements must be respected.
The application is NOT interactive. Tapping on any button or element should not do anything, other than displaying the tap.
You will deliver the source code in a zip package.

You must create 2 reusable components and use them on the screen in the indicated areas:
an AmountText component, highlighted in orange
an AccountTitle component, highlighted in violet

### AmountText ###
For the sake of this exercise, we will use amounts in Euro only.
However, the component must follow these rules:
it can be configured to display either the € symbol, or the EUR iso code
the configuration is global, i.e. applied once when the application starts
a negative amount must be displayed in red with a minus prefix (e.g. -€500)

### AccountTitle ###
There are no specific requirements for this component.

### Restrictions ###
You must NOT use SwitfUI.
You must NOT use any 3rd party dependency.




