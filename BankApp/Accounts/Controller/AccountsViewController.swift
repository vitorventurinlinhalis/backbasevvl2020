//
//  FirstViewController.swift
//  BankApp
//
//  Created by Vitor Venturin Linhalis on 15/08/2020.
//  Copyright © 2020 Vitor Venturin Linhalis. All rights reserved.
//

import UIKit

class AccountsViewController: UIViewController {
    @IBOutlet private weak var amountTextView: AmountTextView!
    @IBOutlet weak var accountTableView: UITableView!
    private var amountTextViewModel: AmountTextViewModel!
    var results: [Result]!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupBalance()
        setupMockData()
        setupTableView()
    }
    
    private func setupTableView() {
        accountTableView.dataSource = self
        accountTableView.delegate = self
        accountTableView.isScrollEnabled = false
    }
    
    private func setupBalance() {
        let amount = AmountText(value: 100.00, currencyCode: "EUR", currencySymbol: "€")
        self.amountTextViewModel = AmountTextViewModel(amount: amount)
        self.amountTextView.amountTextLabel.font = Constants.Font.bigAmountFont
        self.amountTextView.amountTextLabel.textAlignment = .center
        self.amountTextView.amountTextLabel.setAmountFormattedText(text: self.amountTextViewModel.currencyString)

        amountTextViewModel.refreshUI = {
            DispatchQueue.main.async {
                self.amountTextView.amountTextLabel.setAmountFormattedText(text: self.amountTextViewModel.currencyString)
                self.amountTextView.amountTextLabel.textColor = self.amountTextViewModel.textColor
            }
        }
    }
    
    private func generateAccount(by name: String, iban: String, balance: Decimal, code: String, symbol: String) -> Account {
        let accountTitle = AccountTitle(name: name, number: iban)
        let amountText = AmountText(value: balance, currencyCode: code, currencySymbol: symbol)
        let account = Account(title: accountTitle, balance: amountText)
        return account
    }
    
    private func setupMockData() {
        let a1 = generateAccount(by: "Current account", iban: "NL25 TRIO 0269 8445 22", balance: 1500.00, code: "EUR", symbol: "€")
        let a2 = generateAccount(by: "Joint account", iban: "NL81 TRIO 2698 4456 80", balance: 1000.00, code: "EUR", symbol: "€")
        let a3 = generateAccount(by: "Savings account", iban: "NL27 ABNA 0212 4710 66", balance: -10.00, code: "EUR", symbol: "€")
        let a4 = generateAccount(by: "My term deposit", iban: "NL55 ABNA 2040 50", balance: 13210.11, code: "EUR", symbol: "€")
        let a5 = generateAccount(by: "Our home loan", iban: "NL28 TRIO 9400 6447 50﻿", balance: 150000.00, code: "EUR", symbol: "€")
        
        results = [Result(section: Section(title: "Current Accounts", totalAmount: 2500.00), accounts: [a1, a2]),
                    Result(section: Section(title: "Saving Accounts", totalAmount: -10.00), accounts: [a3]),
                    Result(section: Section(title: "Term Deposits", totalAmount: 10000.00), accounts: [a4]),
                    Result(section: Section(title: "Loans", totalAmount: 150000.00), accounts: [a5])]
    }
}

extension AccountsViewController: UITableViewDataSource {
    // MARK: - Table View DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return results.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results[section].accounts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AccountListCell = tableView.dequeueReusableCell(withIdentifier: Constants.Table.ReuseIdentifier.accountCellId, for: indexPath) as! AccountListCell
        let account = results[indexPath.section].accounts[indexPath.row]
        
        let accountTitleViewModel = AccountTitleViewModel(accountTitle: account.title)
        let amountTextViewModel = AmountTextViewModel(amount: account.balance)
        cell.amountTextView.amountTextLabel.textColor = amountTextViewModel.textColor
        
        cell.accountTitleView.nameLabel.text = accountTitleViewModel.name
        cell.accountTitleView.numberLabel.text = accountTitleViewModel.number
        cell.amountTextView.amountTextLabel.setAmountFormattedText(text: amountTextViewModel.currencyString)
        return cell
    }
}

extension AccountsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = HeaderView(frame: CGRect(x: 0.0, y: 0.0, width: tableView.bounds.width, height: Constants.Table.Height.value))
        
        if let sectionLabel = headerView.sectionNameLabel {
            sectionLabel.font = Constants.Font.smallAmountFont
            sectionLabel.text = results[section].section.title
        }

        let amountText = AmountText(value: results[section].section.totalAmount, currencyCode: "EUR", currencySymbol: "€")
        let amountTextViewModel = AmountTextViewModel(amount: amountText)
        if let amountLabel = headerView.totalAmountTextView.amountTextLabel {
            amountLabel.font = Constants.Font.smallAmountFont
            amountLabel.setAmountFormattedText(text: amountTextViewModel.currencyString)
            amountLabel.textColor = amountTextViewModel.textColor
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Constants.Table.Height.value
    }
}

struct Result {
    let section: Section
    let accounts: [Account]
}

final class ContentSizedTableView: UITableView {
    override var contentSize: CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}
