//
//  Amount.swift
//  BankApp
//
//  Created by Vitor Venturin Linhalis on 16/08/2020.
//  Copyright © 2020 Vitor Venturin Linhalis. All rights reserved.
//

import Foundation

struct AmountText { 
    var value: Decimal
    var currencyCode: String
    var currencySymbol: String
}
