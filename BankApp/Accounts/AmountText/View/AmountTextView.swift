//
//  AccountText.swift
//  BankApp
//
//  Created by Vitor Venturin Linhalis on 15/08/2020.
//  Copyright © 2020 Vitor Venturin Linhalis. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable open class AmountTextView: UIView {
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var amountTextLabel: UILabel!
    let nibName: String = "AmountTextView"

    func setup() {
        self.view = UINib(nibName: self.nibName, bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as? UIView 
        self.view.frame = bounds
        self.addSubview(self.view)
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
}
