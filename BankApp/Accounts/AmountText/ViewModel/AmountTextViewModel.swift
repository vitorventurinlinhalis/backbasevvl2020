//
//  AmountTextViewModel.swift
//  BankApp
//
//  Created by Vitor Venturin Linhalis on 15/08/2020.
//  Copyright © 2020 Vitor Venturin Linhalis. All rights reserved.
//

import Foundation
import UIKit

struct Formatter {
    static let numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "eu_EU") // Production: Locale.current
        formatter.numberStyle = .currency
        return formatter
    }()
}

class AmountTextViewModel {
    public var refreshUI: (() -> Void)?
    var amount: AmountText
    var currencyString: String! {
        didSet {
            refreshUI?()
        }
    }
    var textColor: UIColor = UIColor.black
    
    func sanitizedCurrencyString() -> String? {
        return currencyString?.replacingOccurrences(of: "\u{00A0}", with: "")
    }
    
    // MARK: - Object Lifecycle:
    init(amount: AmountText) {
        self.amount = amount
        let numberFormatter = Formatter.numberFormatter
        numberFormatter.currencySymbol = amount.currencySymbol
        numberFormatter.currencyCode = amount.currencyCode
        self.setAmount(to: amount.value)
    }
    
    func setAmount(to newValue: Decimal) {
        self.amount.value = newValue
        let numberFormatter = Formatter.numberFormatter
        self.currencyString = numberFormatter.string(from: amount.value as NSNumber)
        if amount.value < 0.0 {
            self.textColor = UIColor.red
        } else {
            self.textColor = UIColor.black
        }
    }
}
