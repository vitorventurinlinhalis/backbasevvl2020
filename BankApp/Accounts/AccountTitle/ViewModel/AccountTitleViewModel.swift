//
//  AccountTitleViewModel.swift
//  BankApp
//
//  Created by Vitor Venturin Linhalis on 18/08/2020.
//  Copyright © 2020 Vitor Venturin Linhalis. All rights reserved.
//

import Foundation

struct AccountTitleViewModel {
    var name: String?
    var number: String?
    
    private let accountTitle: AccountTitle?
    
    init(accountTitle: AccountTitle) {
        self.accountTitle = accountTitle
        
        self.name = accountTitle.name
        self.number = accountTitle.number
    }
}
