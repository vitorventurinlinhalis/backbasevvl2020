//
//  AccountTitle.swift
//  BankApp
//
//  Created by Vitor Venturin Linhalis on 15/08/2020.
//  Copyright © 2020 Vitor Venturin Linhalis. All rights reserved.
//

import Foundation

struct AccountTitle {
    var name: String
    var number: String
}
