//
//  AmountListCell.swift
//  BankApp
//
//  Created by Vitor Venturin Linhalis on 18/08/2020.
//  Copyright © 2020 Vitor Venturin Linhalis. All rights reserved.
//

import Foundation
import UIKit

class AccountListCell: UITableViewCell {
    @IBOutlet weak var accountTitleView: AccountTitleView!
    @IBOutlet weak var amountTextView: AmountTextView!
}
