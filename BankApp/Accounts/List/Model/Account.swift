//
//  Account.swift
//  BankApp
//
//  Created by Vitor Venturin Linhalis on 18/08/2020.
//  Copyright © 2020 Vitor Venturin Linhalis. All rights reserved.
//

import Foundation

struct Account {
    var title: AccountTitle
    var balance: AmountText
}
