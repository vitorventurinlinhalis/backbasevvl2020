//
//  Section.swift
//  BankApp
//
//  Created by Vitor Venturin Linhalis on 19/08/2020.
//  Copyright © 2020 Vitor Venturin Linhalis. All rights reserved.
//

import Foundation

struct Section: Hashable {
    var title: String
    var totalAmount: Decimal
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(title)
        hasher.combine(totalAmount)
    }
    
    static func == (lhs: Section, rhs: Section) -> Bool {
        return lhs.title == rhs.title && lhs.totalAmount == rhs.totalAmount
    }
}
