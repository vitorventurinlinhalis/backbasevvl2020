//
//  HeaderView.swift
//  BankApp
//
//  Created by Vitor Venturin Linhalis on 19/08/2020.
//  Copyright © 2020 Vitor Venturin Linhalis. All rights reserved.
//

import UIKit

@IBDesignable open class HeaderView: UIView {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var sectionNameLabel: UILabel!
    @IBOutlet weak var totalAmountTextView: AmountTextView!
    let nibName: String = "HeaderView"
    
    func setup() {
        self.view = UINib(nibName: self.nibName, bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as? UIView
        self.view.frame = bounds
        self.addSubview(self.view)
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
}
