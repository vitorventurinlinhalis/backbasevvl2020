//
//  UILabel+Amount.swift
//  BankApp
//
//  Created by Vitor Venturin Linhalis on 18/08/2020.
//  Copyright © 2020 Vitor Venturin Linhalis. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    func setAmountFormattedText(text: String) {
        let split = text.split(separator: Character(Formatter.numberFormatter.currencyDecimalSeparator))
        guard split.count > 1 else {
            return
        }
        let font = self.font!
        let subscriptFont = font.withSize(font.pointSize * 0.7)
        let subscriptOffset = font.pointSize * 0.3
        let attributedString = NSMutableAttributedString(string: text,
                                                         attributes: [.font : font])
        let decimalString = split[1]
        let index: Int = text.distance(from: text.startIndex, to: decimalString.startIndex)
        let range = NSRange(location: index, length: 2)
        attributedString.setAttributes([.font: subscriptFont, .baselineOffset: subscriptOffset], range: range)

        self.attributedText = attributedString
    }
}


