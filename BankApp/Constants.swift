//
//  Constants.swift
//  BankApp
//
//  Created by Vitor Venturin Linhalis on 20/08/2020.
//  Copyright © 2020 Vitor Venturin Linhalis. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    struct Theme {
        static let primaryColor: UIColor = .blue
    }
    struct Font {
        static let bigAmountFont: UIFont = .systemFont(ofSize: 26)
        static let smallAmountFont: UIFont = .systemFont(ofSize: 14)
    }
    
    struct Table {
        struct ReuseIdentifier {
            static let accountCellId = "accountCellId"
            static let tableHeaderId = "headerId"
        }
        struct Height {
            static let value: CGFloat = 40.0
        }
    }
}
